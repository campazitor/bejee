<?php
error_reporting(E_ALL);

use src\Base\Router;

define('ROOT', dirname(__DIR__));
define('ROUTES', ROOT.'/config/routes.php');
define('DB', ROOT.'/config/database.php');
require __DIR__.'/../vendor/autoload.php';

session_start();

$router = new Router();
$router->run();