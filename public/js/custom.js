$(document).ready(function ($) {

    $("body")
        .on('submit', "form#auth", function (e) {
            e.preventDefault();
            let that = this;
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    $(that).find('span').remove();
                    if (response.message) {
                        $(that).prepend("<span class='small red'>" + response.message + "</span>");
                    } else {
                        location.reload();
                    }
                },
            });
        })
        .on('submit', "form#task", function (e) {
            $("input").prop('disabled', false);
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.message) {
                        $('.modal-content').html(
                            "<div class='modal-header'><h5 class='modal-title'>" + response.message + "</h5></div>"
                        );
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else
                        $('.modal-content').html(response.content);
                }
            });
        })
        .on("click", "button[data-toggle='modal']", function (e) {
            $.ajax({
                url: $(this).attr('data-action'),
                type: "POST",
                dataType: "json",
                success: function (response) {
                    $('.modal-content').html(response.content);
                }
            });
        });
});