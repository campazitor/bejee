<?php
/**
 * list routes
 */
return [
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    'task/delete/([0-9]+)' => 'task/delete/$1',
    'task/edit/([0-9]+)' => 'task/edit/$1',
    'task/create' => 'task/create',
    'index' => 'task/index',
];