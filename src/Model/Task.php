<?php

namespace src\Model;

use src\Base\Model;

/**
 * Class Task
 * @package src\Model
 */
class Task extends Model
{

    /** @var string */
    protected $table = "tasks";

    /**
     * @return array
     */
    public function getAttr()
    {
        return [
            "name" => [
                "label" => "Имя",
                "type" => "input"
            ],
            "email" => [
                "label" => "Електронная почта",
                "type" => "input"
            ],
            "text" => [
                "label" => "Текст задачи",
                "type" => "text"
            ],
            "done" => [
                "label" => "Статус",
                "type" => "checkbox"
            ],
            "edited" => [
                "label" => "Отредактировано",
                "type" => "checkbox"
            ]
        ];
    }
}