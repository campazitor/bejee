<?php

namespace src\Model;

use src\Base\Model;

/**
 * Class User
 * @package Core\Model
 */
class User extends Model
{
    /** @var string */
    protected $table = "users";
}