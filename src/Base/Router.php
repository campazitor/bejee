<?php

namespace src\Base;

/**
 * Class Router
 * @package src\Base
 */
class Router
{
    /** @var array */
    private $routes;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routes = include ROUTES;
    }

    /**
     * starts the required router
     */
    public function run()
    {
        $uri = $this->getURI();

        foreach ($this->routes as $pattern => $path) {
            if (preg_match("~^$pattern$~", $uri)) {

                $internalRoute = preg_replace("~$pattern~", $path, $uri);
                $segments = explode('/', $internalRoute);
                $route = array_splice($segments, 0, 2);

                $controllerName = 'src\Controllers\\'.ucfirst($route[0]).'Controller';
                $actionName = $route[1];

                if (class_exists($controllerName)) {
                    $object = new $controllerName($route);

                    if (method_exists($object, $actionName)) {
                        call_user_func_array([$object, $actionName], $segments);
                        break;
                    } else {
                        require ROOT."/src/Views/error/404.php";
                    }
                } else {
                    require ROOT."/src/Views/error/404.php";
                }
            }
        }
    }

    /**
     * Returns request string
     *
     * @return string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            if (strstr($_SERVER['REQUEST_URI'], '?', true) == '/' || $_SERVER['REQUEST_URI'] == '/') {
                return 'index';
            } else {
                return trim($_SERVER['REQUEST_URI'], '/');
            }
        }
    }

}