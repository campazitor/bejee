<?php

namespace src\Base;

/**
 * Class Controller
 * @package src\Base
 */
abstract class Controller
{
    /**
     * get the file of the Views
     * @param string $view
     * @param array $data
     * @param string|false $layout
     */
    public function getView($view, $data, $layout = "layout/default.php")
    {
        $object = new View();
        $object->render($view, $data, $layout);
    }
}