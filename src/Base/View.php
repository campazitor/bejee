<?php

namespace src\Base;

/**
 * Class View
 * @package src\Base
 */
class View
{
    /**
     * connecting view templates
     * @param string $view
     * @param array $data
     * @param string $layout
     */
    public function render($view, $data, $layout)
    {
        if (isset($data)) {
            extract($data);
        }

        $file_view = ROOT . "/src/Views/$view" . ".php";

        ob_start();
        if (is_file($file_view)) {
            require $file_view;
        } else {
            echo "<p>Не найден вид <b>{$file_view}</b></p>";
        }
        $content = ob_get_clean();

        if ($layout) {
            $file_layout = ROOT . "/src/Views/$layout";
            if (is_file($file_layout)) {
                require $file_layout;
            } else {
                echo "<p>Не найден шаблон <b>{$file_layout}</b></p>";
            }
        } else {
            echo json_encode(["content" => $content]);
        }

    }
}