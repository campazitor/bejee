<?php

namespace src\Base;

/**
 * Class Model
 * @package src\Base
 */
class Model
{
    /** @var Database */
    protected $pdo;

    /** @var string */
    protected $table;

    /** @var int */
    private $limit = 3;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->pdo = Database::instance();
    }

    /**
     * save new block in database
     * @param array $param
     * @return bool
     */
    public function save($param)
    {
        $sql = "INSERT INTO $this->table SET {$this->getParam($param)}";

        return $this->pdo->execute($sql, $param);
    }

    /**
     * @param $data
     * @param $id
     * @return bool
     */
    public function update($data, $id)
    {
        $sql = "UPDATE $this->table SET {$this->getParam($data)} WHERE id = $id";
        return $this->pdo->execute($sql, $data);
    }

    /**
     * delete block in database
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $sql = "DELETE FROM $this->table WHERE id = $id";

        return $this->pdo->execute($sql);
    }

    /**
     * @param $arFilter
     * @return array
     */
    public function findAll($arFilter)
    {
        $sql = "SELECT * FROM $this->table";

        if ($arFilter["sort"]) {
            $sql .= " ORDER by " . $arFilter["sort"];

            if ($arFilter["order"]) {
                $sql .= " " . $arFilter["order"];
            }
        }

        $sql .= " LIMIT :offset,$this->limit";
        $param["offset"] = ($arFilter["page"] - 1) * $this->limit;

        $this->pdo->execute($sql, $param);

        return [
            "items" => $this->pdo->getAll(),
            "pages" => (int)ceil($this->countPages() / $this->limit)
        ];
    }

    /**
     * @return int
     */
    public function countPages()
    {
        $sql = "SELECT COUNT(*) as count FROM $this->table";
        $this->pdo->execute($sql);
        return (int)$this->pdo->getOne()["count"];
    }

    /**
     * @param $param
     * @return bool|mixed
     */
    public function findBy($param)
    {
        $where = str_replace(",", " AND", $this->getParam($param));
        $sql = "SELECT * FROM {$this->table} WHERE $where";

        $this->pdo->execute($sql, $param);
        return $this->pdo->getOne();
    }


    /**
     * get the parameter string for the query
     * @param array $param
     * @return string
     */
    public function getParam($param)
    {
        $row = '';
        foreach ($param as $name => $value) {
            $row .= "$name = :$name, ";
        }
        $row = rtrim(rtrim($row), ',');

        return $row;
    }
}