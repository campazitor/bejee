<?php

namespace src\Base;

/**
 * Class Database
 * @package src\Base
 */
class Database
{
    /** @var \PDO */
    protected $pdo;

    /** @var */
    protected static $instance;

    /** @var  */
    protected $result;

    /** @var \PDOStatement*/
    protected $stmt;

    /**
     * Database constructor.
     */
    protected function __construct()
    {
        $db = require DB;
        $option = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        ];
        $this->pdo = new \PDO($db['dsn'], $db['task'], $db['password'], $option);
    }

    /**
     * connection to the database
     * @return Database
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * @param $sql
     * @param null $param
     * @return bool
     */
    public function execute($sql, $param = null)
    {
        $this->stmt = $this->pdo->prepare($sql);
        if (isset($param)) {
            foreach ($param as $key => $value) {
                $this->stmt->bindValue(
                    ":$key",
                    $value,
                    is_string($value) ? \PDO::PARAM_STR : \PDO::PARAM_INT);
            }
        }

        $this->result = $this->stmt->execute();

        return $this->result;
    }

    /**
     * @return array|bool
     */
    public function getAll()
    {
        if ($this->result !== false) {
            return $this->stmt->fetchAll();
        }

        return false;
    }

    /**
     * @return bool|mixed
     */
    public function getOne()
    {
        if ($this->result !== false) {
            return $this->stmt->fetch();
        }

        return false;
    }
}