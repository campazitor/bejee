<?php

namespace src\Controllers;

use src\Base\Controller;
use src\Model\Task;

/**
 * Class TaskController
 * @package src\Controllers
 */
class TaskController extends Controller
{
    /**
     * list tasks
     */
    public function index()
    {
        /** @var Task $model */
        $model = new Task();

        $title = [
            "title" => "Список задач",
            "titlesTable" => $model->getAttr()
        ];

        $arFilter = [
            "page" => isset($_GET['page']) ? intval($_GET['page']) : 1,
            "sort" => isset($_GET['sort']) ? $_GET['sort'] : null,
            "order" => isset($_GET['order']) ? $_GET['order'] : null
        ];

        $records = $model->findAll($arFilter);

        $this->getView(
            "task/index",
            compact("title","arFilter", "records")
        );
    }

    /**
     * create task
     */
    public function create()
    {
        /** @var Task $model */
        $model = new Task();

        $title = "Добавить задачу";
        $button = "Добавить";
        $action = "task/create";
        $fields = $model->getAttr();
        $task = null;

        if (!empty($_POST)) {
            $validation = $this->validate($_POST, $fields);
            $task = $validation["task"];
            if ($validation["status"]) {
                $model->save($task);
                echo json_encode(["message" => "Задача успешно добавлена"]);
                exit();
            }
        }

        $this->getView("task/form", compact('title', 'button', 'action', 'fields', 'task'), null);
    }

    /**
     * edit task
     * @param $id
     */
    public function edit($id)
    {
        /** @var Task $model */
        $model = new Task();
        $task = $model->findBy(["id" => $id]);

        $title = "Редактировать задачу";
        $button = "Редактировать";
        $action = "task/edit/$id";
        $fields = $model->getAttr();

        if (!empty($_POST)) {
            if (!isset($_SESSION['admin'])){
                echo json_encode(["message" => "Редактировать задачи могут только авторизованные пользователи"]);
                exit();
            }
            $validation = $this->validate($_POST, $fields);
            $data = $validation["task"];
            if ($validation["status"]) {
                if ($task["text"] != $data["text"])
                    $data["edited"] = 1;
                $model->update($data, $task['id']);
                echo json_encode(["message" => "Задача успешно отредактирована"]);
                exit();
            }
        }

        $this->getView("task/form", compact('title', 'task', 'fields', 'button', 'action'), null);
    }

    /**
     * delete task
     * @param int $id
     */
    public function delete($id)
    {
        /** @var Task $model */
        $model = new Task();

        $model->delete($id);
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }

    /**
     * @param $post
     * @param $fields
     * @return array
     */
    private function validate($post, &$fields)
    {
        $fieldsNew = [
            "status" => true,
            "task" => []
        ];

        foreach ($fields as $k => $value) {
            if($k == "edited") continue;
            if (isset($post[$k]) && $post[$k]) {
                if ($k == "email" && !filter_var(htmlspecialchars(trim($post[$k])), FILTER_VALIDATE_EMAIL)) {
                    $fields[$k]["error"] = "Введите пожалуйста корректный електронный адрес";
                    $fieldsNew["status"] = false;
                    continue;
                }
                $fieldsNew["task"][$k] = htmlspecialchars(trim($post[$k]));
            } else {
                if($k == "done") {
                    $fieldsNew["task"][$k] = 0;
                    continue;
                }
                $fields[$k]["error"] = "Это поле необходимо заполнить";
                $fieldsNew["status"] = false;
            }
        }

        return $fieldsNew;
    }
}