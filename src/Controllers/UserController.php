<?php

namespace src\Controllers;

use src\Base\Controller;
use src\Model\User;

class UserController extends Controller
{
    public function login()
    {
        /** @var User $model */
        $model = new User();

        $login = htmlspecialchars(trim($_POST['login'])) ?: null;
        $password = htmlspecialchars(trim($_POST['password'])) ?: null;

        if ($login && $password) {
            $data = [
                "name" => $login,
                "password" => $password
            ];
            $user = $model->findBy($data);
            if ($user) {
                $_SESSION['admin'] = $user;
                echo true;
            } else {
                echo json_encode(["message" => "Некорректный логин и пароль"]);
            }
        } else {
            echo json_encode(["message" => "Пожалуйста заполните логин и пароль"]);
        }
    }

    public function logout()
    {
        unset($_SESSION["admin"]);
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
}