<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/custom.css">
    <title><?= $title["title"] ?></title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-secondary" role="navigation">
    <div class="container">
        <div class="collapse navbar-collapse" id="exCollapsingNavbar">
            <ul class="nav navbar-nav flex-row justify-content-between ml-auto">
                <?php if (isset($_SESSION['admin'])): ?>
                    <li class="user">Пользователь - <span class="green"><?= $_SESSION['admin']['name']?></span></li>
                    <li class="px-3 py-2">
                        <a href="/user/logout" class="logout">Выйти</a>
                    </li>
                <?php else: ?>
                <li class="dropdown order-1">
                    <button type="button" id="dropdownMenu1" data-toggle="dropdown" class="btn btn-outline-secondary dropdown-toggle login">Войти <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right mt-2">
                        <li class="px-3 py-2">
                            <form id="auth" method="post" action="/user/login">
                                <div class="form-group">
                                    <input name="login" placeholder="имя" class="form-control form-control-sm" type="text">
                                </div>
                                <div class="form-group">
                                    <input name="password" placeholder="пароль" class="form-control form-control-sm" type="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Авторизоваться</button>
                                </div>
                            </form>
                        </li>

                    </ul>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<?= $content ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" defer></script>
<script src="/js/bootstrap.min.js" defer></script>
<script src="/js/custom.js" defer></script>
</body>
</html>


