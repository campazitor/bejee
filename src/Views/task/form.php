<div class="modal-header">
    <h5 class="modal-title"><?= $title ?></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form method="post" id="task" action="<?= $action ?>">
    <div class="modal-body">
        <?php if (isset($task["id"])): ?>
            <input type="hidden" name="id" value="<?= $task['id'] ?>">
        <?php endif; ?>
        <?php foreach ($fields as $key => $value): ?>
            <?php if ($key == 'edited') continue; ?>
            <?php if ($key == 'done' && !isset($_SESSION['admin'])) continue; ?>
            <div class="form-group">
                <label for="<?= $key ?>"><?= $value["label"] ?></label>
                <?php if ($value["type"] == "text"): ?>
                    <textarea class="form-control" id="<?= $key ?>" name="<?= $key ?>"><?= isset($task[$key]) ? $task[$key] : "" ?></textarea>
                <?php else: ?>
                    <input class="<?= $value["type"] == "checkbox" ? "form-check-input" : "form-control" ?>"
                           id="<?= $key ?>"
                           type="<?= $value["type"] ?>"
                           name="<?= $key ?>"
                           <?= (isset($task[$key]) && $value["type"] != "checkbox") ? "value=" . $task[$key] : "" ?>
                           <?= (isset($task["id"]) && ($key == "name" || $key == "email")) ? "disabled" : "" ?>
                           <?= ($value["type"] == "checkbox" && isset($task[$key]) && $task[$key]) ? "checked": "" ?>
                           <?= ($value["type"] == "checkbox") ? "value='1'": "" ?>
                    />
                <?php endif; ?>
                <?php if (isset($value["error"])): ?>
                    <div class="red small"><?= $value["error"] ?></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary"><?= $button ?></button>
        <button type="button" data-dismiss="modal" class="btn btn-secondary">вернуться на список</button>
    </div>
</form>