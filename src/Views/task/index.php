<?php
/**
 * @param int|null $page
 * @param string|null $sort
 * @param string|null $order
 * @return string
 */
function getRequest($page = null, $sort = null, $order = null)
{
    $request = "/";
    if ($page > 1) {
        $request .= "?page=$page";
    } else {
        $request .= "?page=1";
    }
    if ($sort) {
        $request .= "&sort=$sort";
    }
    if ($order) {
        $request .= "&order=$order";
    }

    return $request;
}

?>
<div class="container">
    <div class="row">
        <table class="table">
            <caption><h1><?= $title["title"] ?></h1></caption>
            <thead>
            <tr>
                <?php foreach ($title["titlesTable"] as $k => $v): ?>
                <?php if ($k == 'edited' && !isset($_SESSION['admin'])) continue; ?>
                    <th scope="col" nowrap><?= $v["label"] ?>
                        <a href="<?= getRequest($arFilter["page"], $k, "ASC") ?>">
                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                        </a>
                        <a href="<?= getRequest($arFilter["page"], $k, "DESC") ?>">
                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                        </a>
                    </th>
                <?php endforeach; ?>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($records["items"] as $record): ?>
                <tr>
                    <?php foreach ($title["titlesTable"] as $k => $v): ?>
                        <?php if ($k == 'done'): ?>
                            <td><?= $record[$k] ? "Выполнено" : "" ?></td>
                        <?php elseif ($k == 'edited'): ?>
                            <?php if (isset($_SESSION['admin'])): ?>
                            <td><?= $record[$k] ? "Отредактировано" : "" ?></td>
                            <?php endif; ?>
                        <?php else: ?>
                            <td><?= $record[$k] ?></td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if (isset($_SESSION['admin'])): ?>
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#task"
                                data-action="/task/edit/<?= $record['id'] ?>" data-id="<?= $record['id']?>">
                            изменить задачу
                        </button>
                    </td>
                    <td>
                        <a href="task/delete/<?= $record['id'] ?>" style="white-space: nowrap"
                           onclick="return confirm('Запись будет удалена безвозвратно!!!');">
                            удалить задачу
                        </a>
                    </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="col-sm-12">
            <?php if ($records["pages"] > 1): ?>
                <ul class="pagination">
                    <?php for ($i = 1; $i <= $records["pages"]; $i++): ?>
                        <li class="page-item">
                            <a class="page-link"
                               href="<?= getRequest($i, $arFilter["sort"], $arFilter["order"]) ?>"><?= $i ?>
                            </a>
                        </li>
                    <?php endfor; ?>
                </ul>
            <?php endif; ?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#task"
                    data-action="task/create">
                Добавить задачу
            </button>
        </div>
    </div>
</div>
<div class="modal fade" id="task" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>